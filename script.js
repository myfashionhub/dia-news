function Articles() {
    this.newsApiUrl = 'https://newsapi.org/v2/everything?apiKey=3b134d79127640af972e69bd51936f24&language=en';

    this.init = () => {
        $('.error-message').html('Welcome to Dia News! Enter a search term to view articles.');

        $('.article-search').submit((e) => {
            e.preventDefault();
            this.onSearch();
        });
    };

    this.onSearch = () => {
        $('.article-results').empty();
        $('.error-message').empty();

        const searchTerm = $('.article-search input').val();
        if (!searchTerm) {
            $('.error-message').html('Please enter a search term');
        } else {
            this.searchArticles(searchTerm);
        }
    };

    this.searchArticles = (searchTerm) => {
        let url = `${this.newsApiUrl}&q=${searchTerm}`;

        const sort = $('.sort option:selected').val();
        if (sort) {
            url += `&sortBy=${sort}`;
        }

        $.ajax({
            url: url,
            type: 'GET',
            success: (data) => {
                this.displayArticles(data.articles);
            },
            error: (error) => {
                $('.error-message').html('An error occurred while fetching articles.');
            },
        });
    };

    this.displayArticles = (articles) => {
        articles.forEach((data) => {
            const articleLi = $('<li>').attr('class', 'article');
            const image = $('<img>').attr('src', data.urlToImage);

            const content = $('<div>').attr('class', 'content');
            const title = $('<h3>').html(data.title);
            const desc = $('<p>').html(data.description);
            const readMore = $('<a>').html('Read More').attr('href', data.url).attr('target', '_blank');
            content.append(title).append(desc).append(readMore);

            articleLi.append(image).append(content);
            $('.article-results').append(articleLi);
        });
    };

    this.init();
}

$(function() {
    new Articles();
});
