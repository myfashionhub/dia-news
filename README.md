# Dia News

### How to view
- There are two ways to view the assignment:

- Run a simple web server in this directory:
```
$ python -m SimpleHTTPServer
```
Open `localhost:8000` in a browser.

- Or, you can right click on `index.html` and open it in a browser.

### How to use
- Type a search term in the search bar, e.g. "chef", and hit Enter. The body should be populated with the search results.

- Change the sorting in the "Sort Articles" dropdown, and click Search. The articles' order should change.

- Clicking on "Read More" should open an article in a new browser tab.

- Clearing the search term and hitting Enter should show an error message prompting you to enter a search term.

- I tested on Chrome, Safari and Firefox.

- Limitation: Most of the articles have standard image ratios and text length. However, some of them have taller pictures or descriptions that are too long, resulting in the Read More button being cut off. There are a few fixes for this, depending on the product requirements. We can either truncate the descriptions that are too long, or increase the tile height to fit the content. To handle irregularly sized images, we can make the images divs (instead of img tags), and render the images as the background (`background-size: contain` can make the image fill the div).
